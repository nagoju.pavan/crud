package com.example.crud.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.crud.model.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Integer>{

}
