package com.example.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.crud.model.Ticket;
import com.example.crud.repository.TicketRepository;

@RestController
@RequestMapping("/ticket")
public class Controller {

	@Autowired
	private TicketRepository dao;
	
	@PostMapping("/book")
	public String bookticket(@RequestBody List<Ticket> ticket) {
		dao.saveAll(ticket);
		return "Bookied ticket:" +ticket.size();
		
	}
	
	@GetMapping("/read")
	public  List<Ticket> getTickets(){
		return (List<Ticket>) dao.findAll();
	}
	
}
